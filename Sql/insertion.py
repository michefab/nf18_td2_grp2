personnel_insertion = '''insert INTO personnel 
VALUES('1', 'Haruno', 'Sakura', '2000-01-24', '78530', 'Konoha', 'Rue Charle de Gaulle', '0646789142');
insert INTO personnel(id_personnel, nom, prenom, date_naiss, code_postal, ville, rue) 
VALUES('2', 'Hokage', 'Tsunade', '1970-04-28', '78530', 'Konoha', 'Rue du Palais'); 
insert INTO personnel 
VALUES('3', 'Uchiwa', 'Hitachi', '1993-05-24', '77523', 'Clan Uchiwa', 'Place etoile', '0646775342');
insert INTO personnel(id_personnel, nom, prenom, date_naiss, code_postal, ville, rue) 
VALUES('4', 'Hatake', 'Kakashi', '1983-04-18', '75200', 'Paris', 'Rond point'); 
insert INTO personnel(id_personnel, nom, prenom, date_naiss, code_postal, ville, rue) 
VALUES('5', 'Hermite', 'Jiraya', '1963-08-18', '75200', 'Paris', 'Boulevard Saint Augustin'); 
'''

assistant_insertion = '''INSERT INTO veterinaire VALUES('1');
INSERT INTO veterinaire VALUES('2');'''

veterinaire_insertion = '''INSERT INTO veterinaire VALUES('3');
INSERT INTO veterinaire VALUES('4');
INSERT INTO veterinaire VALUES('5');'''


client_insertion = '''insert INTO client 
VALUES('1', 'Uzumaki', 'Naruto', '2001-06-20', '78530', 'Konoha', 'Rue des moulins', '0654863142'); 
insert INTO client(id_client, nom, prenom, date_naiss, code_postal, ville, rue)
VALUES('2', 'Uchiwa', 'Sasuke', '2000-12-04', '78530', 'Konoha', 'Rue Georges V'); 
insert INTO client 
VALUES('3', 'Uzumaki', 'Minata', '1980-06-17', '78530', 'Konoha', 'Rue des asperges', '0654878242'); 
insert INTO client(id_client, nom, prenom, date_naiss, code_postal, ville, rue)
VALUES('6', 'Snake', 'Orochimaru', '1978-12-23', '69730', 'Lyon', 'Impasse du chateau'); 
insert INTO client(id_client, nom, prenom, date_naiss, code_postal, ville, rue)
VALUES('5', 'Chief', 'Pain', '1986-12-25', '28785', 'Gif sur Yvettes', 'Rue Pape'); 
'''

espece_insertion = '''INSERT INTO espece(nom) VALUES('oiseau'); 
INSERT INTO espece(nom) VALUES('reptile'); 
'''


animal_insertion = '''INSERT INTO animal 
VALUES('1', 'Rex', 'reptile', '{"date_naiss":"2004-02-03","num_puce":"93","num_pass":"78"}'); 
INSERT INTO animal(id_animal, nom, espece) 
VALUES('2', 'Titi', 'oiseau');
INSERT INTO animal 
VALUES('3', 'Pachu', 'reptile', '{"date_naiss":"2004-06-15","num_puce":"50","num_pass":"19"}'); 
INSERT INTO animal(id_animal, nom, espece) 
VALUES('4', 'Grominet', 'oiseau');
INSERT INTO animal 
VALUES('5', 'Pat', 'oiseau', '{"date_naiss":"2014-06-18","num_puce":"50", "num_pass":"25"}'); 
'''
animal_insertion2 = '''INSERT INTO animal 
VALUES("7", "Xer", "oiseau", "{'date_naiss':'2004-04-07','num_puce':'21','num_pass':'44'}"); 
'''

proprietaires_insertion = '''insert INTO proprietaires 
VALUES('2000-05-14', '2020-07-18', '1', '1');
insert INTO proprietaires(date_debut, animal, client) 
VALUES('2000-05-14', '2', '2');
insert INTO proprietaires(date_debut, animal, client)
VALUES('1996-05-24', '3', '6');
insert INTO proprietaires
VALUES('1988-07-14', '2010-08-18', '4', '5');
insert INTO proprietaires(date_debut, animal, client)
VALUES('2021-05-24', '5', '2');
'''

medicament_insertion = '''insert into medicament 
VALUES('Acide propanoïque', 'soigne'); 
insert into medicament 
VALUES('dolicrane', 'contre maux de tête'); 
insert into medicament 
VALUES('Kerosptlate', 'contre les champignons'); 
insert into medicament 
VALUES('Efferalgan', 'reduit la douleur'); 
insert into medicament 
VALUES('Dentifrice', 'soigne les dents'); 
'''

consultation_insertion = '''insert INTO consultation 
VALUES('2023-01-05 11:30:00', '{"observation":"gonflement","procedure":"malaxation","taille":"60","poids":"30"}', '1', '1');
insert INTO consultation(date, personnel, animal) 
VALUES('2023-01-05 09:55:00', '2', '2');
insert INTO consultation 
VALUES('2023-01-05 18:30:00', '{"observation":"fatigue","procedure":"repos","taille":"45","poids":"28"}', '5', '4');
insert INTO consultation(date, personnel, animal) 
VALUES('2023-01-05 14:00:00', '2', '2');
insert INTO consultation 
VALUES('2023-01-05 18:00:00', '{"observation":"toux","procedure":"sirop","taille":"49","poids":"27"}', '1', '2');
insert INTO consultation 
VALUES('2023-01-05 20:00:00', '{"observation":"allergie","procedure":"antibio","taille":"65","poids":"40"}', '3', '1');
'''

autorisation_insertion = '''
insert INTO autorisation VALUES('Acide propanoïque', 'oiseau');
insert INTO autorisation VALUES('dolicrane', 'oiseau');
insert INTO autorisation VALUES('Dentifrice', 'oiseau');
insert INTO autorisation VALUES('Acide propanoïque', 'reptile');
insert INTO autorisation VALUES('Efferalgan', 'oiseau');
insert INTO autorisation VALUES('Kerosptlate', 'oiseau');
'''

traitement_insertion = '''
insert INTO traitement 
VALUES('1', '2002-05-30', '2010-12-24', '1', '1');
insert INTO traitement      
VALUES('2', '2008-07-30', '2015-10-24', '2', '3');
insert INTO traitement 
VALUES('3', '2012-05-30', '2018-07-27', '5', '1');
insert INTO traitement 
VALUES('4', '2019-07-30', '2023-08-24', '3', '4');
'''

prescription_insertion = '''
insert INTO prescription VALUES('10', '1', 'Acide propanoïque');
insert INTO prescription VALUES('75', '2', 'dolicrane');
insert INTO prescription VALUES('200', '3', 'Acide propanoïque');
insert INTO prescription VALUES('5', '4', 'Efferalgan');
'''

analyse_insertion = '''
insert INTO Resultat_Analyse
VALUES('2023-01-05 15:24:00', 'https.unlien.fr', '1', '1');
insert INTO Resultat_Analyse
VALUES('2023-01-05 15:24:00', 'https.unautrelien.fr', '2', '2');
insert INTO Resultat_Analyse
VALUES('2023-01-05 15:24:00', 'https.encoreunlien.fr', '5', '4');   
'''

specialite_insertion = '''
insert INTO specialite VALUES('1', 'reptile'); 
insert INTO specialite VALUES('2', 'reptile'); 
insert INTO specialite VALUES('3', 'oiseau'); 
insert INTO specialite VALUES('4', 'oiseau'); 
insert INTO specialite VALUES('5', 'oiseau'); 
'''

veterinaire_resp_insertion = '''
insert INTO Veterinaire_responsable
VALUES('1998-02-18', '2010-05-22', '1', '4'); 
insert INTO Veterinaire_responsable
VALUES('2006-05-18', '2018-05-12', '2', '3'); 
insert INTO Veterinaire_responsable(date_debut, animal, veterinaire)
VALUES('2016-05-19', '3', '4'); 
insert INTO Veterinaire_responsable(date_debut, animal, veterinaire)
VALUES('2019-05-19', '4', '5'); 
insert INTO Veterinaire_responsable(date_debut, animal, veterinaire)
VALUES('2019-05-19', '2', '4'); 
'''
