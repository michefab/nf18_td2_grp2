#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 11:59:28 2022

@author: chenweimi
"""
import psycopg2
import select
import datetime

def showTables(conn):
    cur = conn.cursor()
    cur.execute(f"select tablename from pg_tables where schemaname = 'public';")
    row = cur.fetchall()
    print(">>>>>>>>>>    Toutes les tables    <<<<<<<<<<")
    for table in row: 
        print("- ", table[0])
    # print(row)

def getInput(text):
    response = input (text)
    if (len(response) == 0 or response == None or response == ""):
        return 'NULL'
    else:
        return "'"+ response + "'"

def inserer(conn):
    showTables(conn)
    cur = conn.cursor()
    try:
        table = input("Entrez le nom de table à inserer: ")
        if table == "Animal" or table=="animal":
            id_animal = getInput("Entrez id d'animal: ")
            nom = getInput("Entrez le nom d'animal: ")
            espece = getInput("Entrez l'espèce d'animal: ")
            date_naiss = input("Entrez la date de naissance d'animal: ")
            num_puce = input("Entrez le numéro de puce d'animal: ")
            num_pass = input("Entrez le numéro de passport d'animal: ")
            """if date_naiss=='' and num_puce=='' and num_pass=='':
                informations
            else:
                informations = '{{'
                if date_naiss != "":
                    informations += '"date_naiss":"%s"' %(date_naiss)
                if num_puce != "":
                    if informations!="":
                        informations+=','
                    informations += '"num_puce":"%s"' %(num_puce)
                if num_pass != "":
                    if informations!="":
                        informations+=','
                    informations += ',"num_pass":"%s"' %(num_pass)
                informations += '}}'"""


            informations = '{{"date_naiss":"%s","num_puce":"%s","num_pass":"%s"}}' %(date_naiss, num_puce, num_pass)
            sql = "INSERT INTO Animal(id_animal, nom, espece, informations) VALUES(%s,%s,%s,'%s');" % (id_animal, nom, espece, informations.format())   
            cur.execute(sql)

        elif table == "Espece" or table == "espece":
            nom = getInput("Entrez le nom d'espèce: ")
            sql = "INSERT INTO Espece(nom) VALUES(%s);" % nom
            cur.execute(sql)

        elif table == "Client" or table=="client":
            id_client = getInput("Entrez id de client: ")
            nom = getInput("Entrez le nom de client: ")
            prenom = getInput("Entrez le prénom de client: ")
            date_naiss = getInput("Entrez la date de naissance de client: ")
            code_postal = getInput("Entrez le code postal de client: ")
            ville = getInput("Entrez la ville de client: ")
            rue = getInput("Entrez la rue de client: ")
            num_tel = getInput("Entrez le numéro de téléphone de client: ")
            sql = "INSERT INTO Client(id_client, nom, prenom, date_naiss, code_postal,ville,rue,num_tel) VALUES(%s,%s,%s,%s,%s,%s,%s,%s);" % ( id_client, nom, prenom, date_naiss, code_postal, ville, rue, num_tel)
            cur.execute(sql)

        elif table == "Personnel" or table=="personnel":
            id_personnel = getInput("Entrez id de personnel: ")
            nom = getInput("Entrez le nom de personnel: ")
            prenom = getInput("Entrez le prénom de personnel: ")
            date_naiss = getInput("Entrez la date de naissance de personnel: ")
            code_postal = getInput("Entrez le code postal de personnel: ")
            ville = getInput("Entrez la ville de personnel: ")
            rue = getInput("Entrez la rue de personnel: ")
            num_tel = getInput("Entrez le numéro de téléphone de personnel: ")
            sql = "INSERT INTO Personnel(id_personnel, nom, prenom, date_naiss, code_postal,ville,rue,num_tel) VALUES(%s,%s,%s,%s,%s,%s,%s,%s);" % ( id_personnel, nom, prenom, date_naiss, code_postal, ville, rue, num_tel)
            cur.execute(sql)

        elif table == "Proprietaires" or table=="proprietaires":
            date_debut = getInput("Entrez la date de début de proprietaire: ")
            date_fin = getInput("Entrez la date de fin de proprietaire: ")
            animal = getInput("Entrez id d'animal: ")
            client = getInput("Entrez id de client: ")
            sql = "INSERT INTO Proprietaires(date_debut, date_fin, animal, client) VALUES(%s,%s,%s,%s);" % (date_debut, date_fin, animal, client)
            cur.execute(sql)

        elif table == "Assistant" or table=="assistant":
            id_assis = getInput("Entrez id d'assistant: ")
            sql = "INSERT INTO Assistant(id_assis) VALUES(%s);" % id_assis
            cur.execute(sql)

        elif table == "Veterinaire_responsable" or table=="veterinaire_responsable":
            date_debut = getInput("Entrez la date de début de la responsabilité: ")
            date_fin = getInput("Entrez la date de fin de la responsabilité: ")
            animal = getInput("Entrez id d'animal: ")
            client = getInput("Entrez id de client: ")
            sql = "INSERT INTO Veterinaire_responsable(date_debut, date_fin, animal, client) VALUES(%s,%s,%s,%s);" % ( date_debut, date_fin, animal, client)
            cur.execute(sql)

        elif table == "Veterinaire" or table=="veterinaire":
            id_vete = getInput("Entrez id de vétérinaire")
            sql = "INSERT INTO Veterinaire(id_vete) VALUES(%s);" % id_vete
            cur.execute(sql)

        elif table == "Resultat_Analyse" or table=="resultat_analyse":
            date = getInput("Entrez la date de l'analyse: ")
            lien = getInput("Entrez le lien de l'analyse: ")
            personnel = getInput("Entrez id de personnel: ")
            animal = getInput("Entrez id d'animal: ")
            sql = "INSERT INTO Resultat_Analyse(date, lien, personnel, animal) VALUES(%s,%s,%s,%s);" % ( date, lien, personnel, animal)
            cur.execute(sql)

        elif table == "Traitement" or table=="traitement":
            id_traite = getInput("Entrez id de traitement: ")
            debut_traitement = getInput("Entrez le début de traitement: ")
            date_fin = getInput("Entrez la fin de traitement: ")
            veterinaire = getInput("Entrez id de vétérinaire: ")
            animal = getInput("Entrez id d'animal: ")
            sql = "INSERT INTO Traitement(id_traite, debut_traitement, date_fin, veterinaire, animal) VALUES(%s,%s,%s,%s,%s);" % (id_traite, debut_traitement, date_fin, veterinaire, animal)
            cur.execute(sql)

        elif table == "Autorisation" or table=="autorisation":
            medicament = getInput("Entrez le nom du médicament: ")
            espece = getInput("Entrez l'espèce d'animal: ")
            sql = "INSERT INTO Autorisation(medicament, espece) VALUES(%s,%s);" % ( medicament, espece)
            cur.execute(sql)

        elif table == "Consultation" or table=="consultation":
            date = getInput("Entrez la date de consultation: ")
            observation = input("Entrez l'observation: ")
            procedure = input("Entrez le procédure: ")
            taille = input("Entrez la taille d'animal: ")
            poids = input("Entrez le poids d'animal: ")
            personnel = getInput("Entrez id de personnel: ")
            animal = getInput("Entrez id d'animal: ")
            observations = '{{"observation":"%s","procedure":"%s","taille":"%s","poids":"%s"}}' %(observation, procedure, taille, poids)
            sql = "INSERT INTO Consultation(date, observations, personnel, animal) VALUES(%s,'%s',%s,%s);" % (date, observations.format(), personnel, animal)
            cur.execute(sql)

        elif table == "Medicament" or table=="medicament":
            nom_molecule = getInput("Entrez le nom de la molécule: ")
            description = getInput("Entrez la description: ")
            sql = "INSERT INTO Medicament(nom_molecule, description) VALUES(%s,%s);" % ( nom_molecule, description)
            cur.execute(sql)

        elif table == "Prescription" or table=="prescription":
            quantite = getInput("Entrez la quantité du médicament: ")
            traitement = getInput("Entrez id du traitement: ")
            medicament = getInput("Entrez le nom de la molécule: ")
            sql = "INSERT INTO Prescription(quantite, traitement, medicament) VALUES(%s,%s,%s);" % ( quantite, traitement, medicament)
            cur.execute(sql)

        elif table == "Specialite" or table=="specialite":
            personnel = getInput("Entrez id de personnel: ")
            espece = getInput("Entrez la spécialité: ")
            sql = "INSERT INTO Specialite(personnel, espece) VALUES(%s,%s);" % (personnel, espece)
            cur.execute(sql)
        else:
            print('Valeur Invalide!')

    except psycopg2.DatabaseError as e:
        print("erreur")
        print(e)
        conn.rollback()
    finally:
        conn.commit()


def afficher(conn):
    showTables(conn)
    cur = conn.cursor()
    try:
        table = input("Entrez le nom de table à afficher: ")
        sql = "SELECT * FROM %s;" % table
        cur.execute(sql)
        colnames = [desc[0] for desc in cur.description]
        for c in colnames:
            print("%s" % c, end = "   ")
        print("")
        row = cur.fetchall()
        for r in row:  # afficher
            for i in r:
                print("%s" % i, end = "   ")
            print("")
    except psycopg2.DatabaseError as e:
        print("erreur")
        print(e)
        conn.rollback()



def selectRequest(conn):
    try:
        while True:
            choix = input('''Faites votre choix (0 => quitter, 1 ~ 13 => select): ''')
            cur = conn.cursor()
            if choix == '0':
                print("Quitter")
                break
            elif choix == '1':
                print('''1 =>  Lister les quantités de médicaments consommés pour une période donnée.''')
                debut_periode = input("Entrez le début de la période: ")
                fin_periode = input("Entrez la fin de la période: ")
                sql = select.quantite_medoc(debut_periode, fin_periode)
            elif choix == '2':
                print('''2 =>  Lister le nombre de traitements prescrits au cours d'une période donnée.''')
                debut_periode = input("Entrez le début de la période: ")
                fin_periode = input("Entrez la fin de la période: ")
                sql = select.traitement_prescrit(debut_periode, fin_periode)
            elif choix == '3':
                print('''3 =>  Lister les procédures effectuées sur un animal donné, avec et triées par date.''')
                nom_animal = input("Entrez le nom d'animal: ")
                sql = select.select_3(nom_animal)
            elif choix == '4':
                print('''4 =>  Compter le nombre d'animaux traités groupés par espèce.''')
                sql = select.select_4()
            elif choix == '5':
                print('''5 =>  Lister les animaux ayant appartenus à un client donné, triés par date d'adoption, avec le nom et prénom du client, et les informations d'identification de l'animal, sa date de naissance et son espèce.''')
                nom_client = input("Entrez le nom de client: ")
                prenom_client = input("Entrez le prénom de client: ")
                sql = select.select_5(nom_client, prenom_client)
            elif choix == '6':
                print('''6 =>  Même question, pour les animaux appartenant actuellement au client.''')
                nom_client = input("Entrez le nom de client: ")
                prenom_client = input("Entrez le prénom de client: ")
                sql = select.select_6(nom_client, prenom_client)
            elif choix == '7':
                print('''7 =>  Même question, pour les animaux ayant appartenu mais n'appartenant plus au client.''')
                nom_client = input("Entrez le nom de client: ")
                prenom_client = input("Entrez le prénom de client: ")
                sql = select.select_7(nom_client, prenom_client)
            elif choix == '8':
                print('''8 =>  Lister l'évolution de croissance taille et poids d'un animal donné, par ordre chronologique.''')
                nom_animal = input("Entrez le nom d'animal: ")
                sql = select.select_8(nom_animal)
                #  .strftime("%Y-%m-%d %H:%M:%S")
            elif choix == '9':
                print('''9 =>  Lister les traitements subis par un animal donné avec leurs dates, triés chronologiquement, sans plus de détails.''')
                nom_animal = input("Entrez le nom d'animal: ")
                sql = select.select_9(nom_animal)
            elif choix == '10':
                print('''10 =>  Lister les traitements en cours pour un animal donné, avec leurs dates, avec le détail des prescriptions (médicaments et quantités par jour).''')
                nom_animal = input("Entrez le nom d'animal: ")
                sql = select.select_10(nom_animal)
            elif choix == '11':
                print('''11 =>  Lister les membres de personnel spécialisés dans les reptiles, avec leur poste et toutes leurs informations.''')
                sql1 = select.select_11_vete()
                print("Ci-dessous affiche les vétérinaires qui spécialisent les reptiles.")
                cur.execute(sql1)
                row1 = cur.fetchall()
                for r in row1:  # afficher
                     for i in r:
                        print(str(i),end = " ")
                     print("")

                sql = select.select_11_assis()
                print("Ci-dessous affiche les assistants qui spécialisent les reptiles.")

            elif choix == '12':
                print('''12 =>  Afficher la liste des animaux ayant été suivis par un vétérinaire donné au cours du dernier mois.''')
                nom_vete = input("Entrez le nom de veterinaire: ")
                prenom_vete = input("Entrez le prenom de veterinaire: ")
                sql = select.select_12(nom_vete,prenom_vete)
            elif choix == '13':
                print('''13 =>  Afficher la liste des vétérinaires ayant suivi un animal donné, avec et triés par leur date de suivi le plus récent.''')
                animal = input("Entrez le nom d'animal: ")
                sql = select.select_13(animal)
            else:
                print('Valeur Invalide!')
                break

            cur.execute(sql)
            row = cur.fetchall()
            for r in row:  # afficher
                for i in r:
                    print(str(i),end = " ")
                print("")
    except psycopg2.DatabaseError as e:
        print(e)

