# 1
def quantite_medoc(debut_periode, fin_periode):
    return f'''
    SELECT quantite, medicament
    FROM Prescription p
    INNER JOIN Traitement t ON p.traitement = t.id_traite
    WHERE t.debut_traitement > '{debut_periode}' AND t.date_fin < '{fin_periode}'
    GROUP BY medicament, quantite
    '''


# exemple utilisation :
'''debut_periode=datetime.datetime(2006, 1, 1)
fin_periode=datetime.datetime(2020, 1, 1)
curseur.execute(select.quantite_medoc(debut_periode, fin_periode))'''


def traitement_prescrit(debut_periode, fin_periode):
    return f'''
    SELECT COUNT(id_traite) AS nb_traitements
    FROM Traitement 
    WHERE debut_traitement> '{debut_periode}' AND date_fin < '{fin_periode}' 
    '''


def select_3(nom_animal):
    return f'''
    SELECT c.observations->>'procedure' 
    FROM Consultation c
    INNER JOIN Animal a ON c.animal = a.id_animal
    WHERE a.nom = '{nom_animal}' AND c.observations->>'procedure' IS NOT NULL
    ORDER BY c.date
    '''


def select_4():
    return '''
    SELECT a.espece, COUNT(a.id_animal) AS nb_animaux
    FROM Animal a
    GROUP BY a.espece
    '''
 


def select_5(nom_client, prenom_client):
    return f"""
    SELECT c.nom, c.prenom, a.nom, a.informations->>'num_puce', a.informations->>'num_pass', p.date_debut AS date_adoption
    From Client c 
    INNER JOIN Proprietaires p ON c.id_client = p.client
    INNER JOIN Animal a ON p.animal = a.id_animal
    WHERE c.nom = '{nom_client}' AND c.prenom = '{prenom_client}'
    ORDER BY p.date_debut
    """


def select_6(nom_client, prenom_client):
    return f"""
    SELECT c.nom, c.prenom, a.nom, a.informations->>'num_puce', a.informations->>'num_pass', p.date_debut AS date_adoption
    From Client c 
    INNER JOIN Proprietaires p ON c.id_client = p.client
    INNER JOIN Animal a ON p.animal = a.id_animal
    WHERE c.nom = '{nom_client}' AND c.prenom = '{prenom_client}' AND p.date_fin IS NULL
    ORDER BY p.date_debut
    """


def select_7(nom_client, prenom_client):
    return f"""
    SELECT c.nom, c.prenom, a.nom, a.informations->>'num_puce', a.informations->>'num_pass', p.date_debut AS date_adoption
    From Client c 
    INNER JOIN Proprietaires p ON c.id_client = p.client
    INNER JOIN Animal a ON p.animal = a.id_animal
    WHERE c.nom = '{nom_client}' AND c.prenom = '{prenom_client}' 
    AND p.date_fin IS NOT NULL 
    ORDER BY p.date_debut
    """


def select_8(nom_animal):
    return f"""
    SELECT c.date, c.observations->>'taille', c.observations->>'poids'
    FROM Consultation c
    INNER JOIN Animal a ON c.animal = a.id_animal
    WHERE a.nom = '{nom_animal}' AND c.observations->>'taille' IS NOT NULL AND c.observations->>'poids' IS NOT NULL
    ORDER BY c.date
    """


def select_9(nom_animal):
    return f'''
    SELECT t.debut_traitement, t.date_fin
    FROM Traitement t
    INNER JOIN Animal a ON t.animal = a.id_animal 
    WHERE a.nom = '{nom_animal}'
    ORDER BY t.debut_traitement
    '''


def select_10(nom_animal):
    return f'''
    SELECT t.id_traite, p.quantite, p.medicament
    FROM Traitement t 
    INNER JOIN Animal a ON t.animal = a.id_animal 
    INNER JOIN Prescription p ON t.id_traite = p.traitement
    WHERE a.nom = '{nom_animal}' AND t.date_fin > NOW()::date
    '''


def select_11_vete():
    return '''
    SELECT p.id_personnel, p.nom, p.prenom, p.date_naiss, p.code_postal, p.ville, p.rue, p.num_tel  FROM personnel p
    INNER JOIN veterinaire v ON p.id_personnel = v.id_vete
    INNER JOIN specialite s ON p.id_personnel = s.personnel
    WHERE s.espece = 'reptile'
    '''


def select_11_assis():
    return '''
    SELECT p.id_personnel, p.nom, p.prenom, p.date_naiss, p.code_postal, p.ville, p.rue, p.num_tel  FROM personnel p
    INNER JOIN assistant a ON p.id_personnel = a.id_assis
    INNER JOIN specialite s ON p.id_personnel = s.personnel
    WHERE s.espece = 'reptile'
    '''
# Avec python rajouter une colone qui indique si c'est vétérinaire ou assistant


def select_12(nom_vete,prenom_vete):
    return f'''
    SELECT a.nom
    FROM Animal a
    INNER JOIN Consultation c ON a.id_animal = c.animal
    INNER JOIN Personnel p ON C.personnel = p.id_personnel
    INNER JOIN Veterinaire v ON p.id_personnel = v.id_vete
    WHERE c.date > NOW()::date - interval '1 months' AND p.nom = '{nom_vete}' AND p.prenom = '{prenom_vete}'
    GROUP BY a.nom
    '''


def select_13(animal):
    return f'''
    SELECT p.nom, p.prenom, MAX(vr.date_debut) AS date_suivi_plus_recente
    FROM Personnel p
    INNER JOIN Veterinaire v ON p.id_personnel = v.id_vete
    INNER JOIN Veterinaire_responsable vr ON v.id_vete = vr.veterinaire
    INNER JOIN Animal a ON vr.animal = a.id_animal
    WHERE a.nom = '{animal}'
    GROUP BY p.nom, p.prenom
    ORDER BY date_suivi_plus_recente desc
    '''
