animal = '''CREATE TABLE IF NOT EXISTS Animal (
    id_animal INTEGER PRIMARY KEY,
    nom varchar(30) NOT NULL,
    espece type_animal REFERENCES Espece(nom) NOT NULL,
    informations JSON 
);'''

type_animal = '''CREATE TYPE type_animal AS ENUM ('félin', 'oiseau', 'canidé', 'reptile', 'rongeur', 'autre');'''

espece = '''CREATE TABLE IF NOT EXISTS Espece (
    nom type_animal PRIMARY KEY
);'''

client = '''CREATE TABLE IF NOT EXISTS Client (
    id_client INTEGER PRIMARY KEY,
    nom varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL,
    date_naiss DATE NOT NULL,
    code_postal INTEGER NOT NULL,
    ville varchar(40) NOT NULL, 
    rue varchar(40) NOT NULL,
    num_tel varchar(30),
    CHECK (date_naiss < NOW()::date - interval '15 years') 
);'''

personnel = '''CREATE TABLE IF NOT EXISTS Personnel (
    id_personnel INTEGER PRIMARY KEY,
    nom varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL,
    date_naiss DATE NOT NULL,
    code_postal INTEGER NOT NULL,
    ville varchar(40) NOT NULL, 
    rue varchar(40) NOT NULL,
    num_tel varchar(30),
    CHECK (date_naiss < NOW()::date - interval '16 years') 
);'''

proprietaires = '''CREATE TABLE IF NOT EXISTS Proprietaires (
    date_debut DATE NOT NULL,
    date_fin DATE,
    animal INTEGER NOT NULL,
    client INTEGER NOT NULL,
    FOREIGN KEY (animal) REFERENCES Animal(id_animal),
    FOREIGN KEY (client) REFERENCES Client(id_client),
    PRIMARY KEY(animal, client),
    CHECK (date_fin > date_debut) 
);'''

assistant = '''CREATE TABLE IF NOT EXISTS Assistant (
    id_assis INTEGER PRIMARY KEY,
    FOREIGN KEY (id_assis) REFERENCES Personnel(id_personnel)
);'''

veterinaire_responsable = '''CREATE TABLE IF NOT EXISTS Veterinaire_responsable (
    date_debut DATE NOT NULL,
    date_fin DATE,
    animal INTEGER NOT NULL,
    veterinaire INTEGER NOT NULL,
    FOREIGN KEY (animal) REFERENCES Animal(id_animal),
    FOREIGN KEY (veterinaire) REFERENCES Veterinaire(id_vete),
    PRIMARY KEY(animal, veterinaire),
    CHECK (date_fin > date_debut) 
);'''

veterinaire = '''CREATE TABLE IF NOT EXISTS Veterinaire (
    id_vete INTEGER PRIMARY KEY, 
    FOREIGN KEY (id_vete) REFERENCES Personnel(id_personnel) 
);'''

analyse = '''CREATE TABLE IF NOT EXISTS Resultat_Analyse (
    date TIMESTAMP NOT NULL, 
    lien varchar(300) UNIQUE NOT NULL, 
    personnel INTEGER NOT NULL, 
    animal INTEGER NOT NULL, 
    PRIMARY KEY(date, personnel, animal),
    FOREIGN KEY (personnel) REFERENCES Personnel(id_personnel),
    FOREIGN KEY (animal) REFERENCES Animal(id_animal),
    CHECK (date::date = NOW()::date)
);'''

traitement = '''CREATE TABLE IF NOT EXISTS Traitement (
    id_traite INTEGER PRIMARY KEY, 
    debut_traitement DATE NOT NULL, 
    date_fin DATE NOT NULL, 
    veterinaire INTEGER NOT NULL, 
    animal INTEGER NOT NULL, 
    FOREIGN KEY (veterinaire) REFERENCES Veterinaire(id_vete), 
    FOREIGN KEY (animal) REFERENCES Animal(id_animal),
    CHECK (date_fin > debut_traitement) 
);'''

autorisation = '''CREATE TABLE IF NOT EXISTS Autorisation (
    medicament VARCHAR(100) NOT NULL, 
    espece type_animal NOT NULL, 
    FOREIGN KEY (medicament) REFERENCES Medicament(nom_molecule), 
    FOREIGN KEY (espece) REFERENCES Espece(nom),  
    PRIMARY KEY(medicament, espece)
);'''

consultation = '''CREATE TABLE IF NOT EXISTS Consultation (
    date TIMESTAMP NOT NULL,
    observations JSON,
    personnel INTEGER NOT NULL,
    animal INTEGER NOT NULL,
    FOREIGN KEY(personnel) REFERENCES Personnel(id_personnel),
    FOREIGN KEY(animal) REFERENCES Animal(id_animal),
    PRIMARY KEY(date, personnel, animal),
    CHECK (date::date = NOW()::date)
);'''

medicament = '''CREATE TABLE IF NOT EXISTS Medicament (
    nom_molecule varchar(100) PRIMARY KEY,
    description varchar(300) NOT NULL
);'''

prescription = '''CREATE TABLE IF NOT EXISTS Prescription (
    quantite INTEGER NOT NULL,
    traitement INTEGER NOT NULL,
    medicament varchar(100) NOT NULL,
    FOREIGN KEY(traitement) REFERENCES Traitement(id_traite),
    FOREIGN KEY(medicament) REFERENCES Medicament(nom_molecule),
    PRIMARY KEY(traitement, medicament),
    CHECK (quantite > 0)
);'''

specialite = '''CREATE TABLE IF NOT EXISTS Specialite (
    personnel INTEGER NOT NULL,
    espece type_animal NOT NULL,
    FOREIGN KEY(personnel) REFERENCES Personnel(id_personnel),
    FOREIGN KEY(espece) REFERENCES Espece(nom),
    PRIMARY KEY(personnel, espece)
);'''
