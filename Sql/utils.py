import psycopg2
from dotenv import load_dotenv
import os
from tables import *
import delete_tables as delete
import insertion as insert

load_dotenv()

HOST = os.environ.get("HOST")
PORT = os.environ.get("PORT")
DBNAME = os.environ.get("DBNAME")
USER = os.environ.get("USER")
PASSWORD = os.environ.get("PASSWORD")


def delete_all(curseur):
    '''DELETE ALL TABLES'''
    curseur.execute(delete.proprio)
    curseur.execute(delete.resultat_analyse)
    curseur.execute(delete.veto_resp)
    curseur.execute(delete.authorisation)
    curseur.execute(delete.consultation)
    curseur.execute(delete.prescription)
    curseur.execute(delete.traitement)
    curseur.execute(delete.medicament)
    curseur.execute(delete.specialite)
    curseur.execute(delete.assistant)
    curseur.execute(delete.veto)
    curseur.execute(delete.client)
    curseur.execute(delete.personnel)
    curseur.execute(delete.animal)

    curseur.execute(delete.espece)


def create_all(curseur):
    ''' all queries to create tables '''
    # curseur.execute(type_animal);
    curseur.execute(personnel)
    curseur.execute(client)
    curseur.execute(assistant)
    curseur.execute(veterinaire)
    curseur.execute(espece)
    curseur.execute(animal)
    curseur.execute(proprietaires)
    curseur.execute(veterinaire_responsable)
    curseur.execute(analyse)
    curseur.execute(traitement)
    curseur.execute(consultation)
    curseur.execute(medicament)
    curseur.execute(prescription)
    curseur.execute(specialite)
    curseur.execute(autorisation)


def checks(conn):
    curseur = conn.cursor()

    curseur.execute(insert.espece_insertion)
    curseur.execute(insert.personnel_insertion)
    curseur.execute(insert.assistant_insertion)
    curseur.execute(insert.veterinaire_insertion)
    curseur.execute(insert.animal_insertion)
    curseur.execute(insert.client_insertion)
    curseur.execute(insert.proprietaires_insertion)
    curseur.execute(insert.veterinaire_resp_insertion)
    curseur.execute(insert.medicament_insertion)
    curseur.execute(insert.consultation_insertion)
    curseur.execute(insert.autorisation_insertion)
    curseur.execute(insert.traitement_insertion)
    curseur.execute(insert.prescription_insertion)
    curseur.execute(insert.analyse_insertion)
    curseur.execute(insert.specialite_insertion)
