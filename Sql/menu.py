#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 12:47:59 2022

@author: chenweimi
"""

import psycopg2  # Charger le module
import commands
import utils as main


if __name__ == "__main__":
    # Connexion sur la BD
    conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % ( main.DBNAME, main.USER, main.HOST, main.PASSWORD))
    if conn is not None:
        print('Connection established to PostgreSQL.')
    else:
        print('Connection not established to PostgreSQL.')

    main.create_all(conn.cursor())
    conn.commit()

    while True:
        choix = input('Faites votre choix (0 => quitter, 1 => inserer, 2 => afficher 3 => select): ')
        if choix == '0':
            break
        elif choix == '1':
            commands.inserer(conn)
        elif choix == '2':
            commands.afficher(conn)
        elif choix == '3':
            print('''    1 =>  Lister les quantités de médicaments consommés pour une période donnée.
    2 =>  Lister le nombre de traitements prescrits au cours d'une période donnée.
    3 =>  Lister les procédures effectuées sur un animal donné, avec et triées par date.
    4 =>  Compter le nombre d'animaux traités groupés par espèce.
    5 =>  Lister les animaux ayant appartenus à un client donné, triés par date d'adoption, avec le nom et prénom du client, et les informations d'identification de l'animal, sa date de naissance et son espèce.
    6 =>  Même question, pour les animaux appartenant actuellement au client.
    7 =>  Même question, pour les animaux ayant appartenu mais n'appartenant plus au client.
    8 =>  Lister l'évolution de croissance taille et poids d'un animal donné, par ordre chronologique.
    9 =>  Lister les traitements subis par un animal donné avec leurs dates, triés chronologiquement, sans plus de détails.
    10 => Lister les traitements en cours pour un animal donné, avec leurs dates, avec le détail des prescriptions (médicaments et quantités par jour).
    11 => Lister les membres de personnel spécialisés dans les reptiles, avec leur poste et toutes leurs informations.
    12 => Afficher la liste des animaux ayant été suivis par un vétérinaire donné au cours du dernier mois.
    13 => Afficher la liste des vétérinaires ayant suivi un animal donné, avec et triés par leur date de suivi le plus récent.
    0 => Quitter la sélection
    ''')
            commands.selectRequest(conn)
        else:
            print('Valeur Invalide!')
    conn.close()
