# NF18 Groupe2 TD2

## Sujet

<b>Gestion d'une clinique vétérinaire</b>

## Membre

Chenwei MI  
Xavier LEMERLE  
Xingyu DU  
Fabien MICHEL

## Accéder au répertoire de projet

```
cd <your path>/nf18_td2_grp2
```

## Lancer la commande suivante

```
python ./sql/menu.py
```
