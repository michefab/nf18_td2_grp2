<h1>MLD</h1>

<div style="font-size:14px">
<h3><b>Animal</b></h3>(#id_animal:int, nom:string, espece=>Espece)<br/>
 <br>AVEC nom NOT NULL, <br/>PROJECTION(Animal, espece) = PROJECTION(Espece, nom)<br/>
<br/>

<h3><b>Informations</b></h3>(#id_info:int, date_naiss:Date, num_puce:int, num_pass:int，#animal=>Animal)<br>
<br>AVEC id_info LOCAL KEY<br><br>

<h3><b>type_animal</b></h3>({félin, oiseau, canidé, reptile, rongeur, autre})<br/>
<br/>

<h3><b>Client</b></h3>(#id_client:int, nom:string, prenom:string, date_naiss:Date, code_postal:int, ville:string, rue:string, num_tel:string) <br/><br>AVEC nom NOT NULL, prenom NOT NULL, date_naiss NOT NULL, code_postal NOT NULL, ville NOT NULL, rue NOT NULL, num_tel NOT NULL, <br/>INTERSECTION (PROJECTION(Client, id_client), PROJECTION(Personnel, id_personnel)) = {}<br/>
<br/>

<h3><b>Personnel</b></h3>(#id_personnel:int, nom:string, prenom:string, date_naiss:Date, code_postal:int, ville:string, rue:string, num_tel:string)<br/> <br>AVEC nom NOT NULL, prenom NOT NULL, date_naiss NOT NULL, code_postal NOT NULL, ville NOT NULL, rue NOT NULL, num_tel NOT NULL, specialite NOT NULL,<br/> INTERSECTION (PROJECTION(Client, id_client), PROJECTION(Personnel, id_personnel)) = {}<br/>
<br/>

<h3><b>Proprietaires</b></h3>(date_debut:Timestamp, date_fin:Timestamp, #animal=>Animal, #client=>Client) <br/><br>AVEC date_debut NOT NULL, animal UNIQUE, client UNIQUE<br/>PROJECTION(Proprietaires, animal) = PROJECTION(Animal, id_animal),<br/> PROJECTION(Proprietaires, client) = PROJECTION(Client, id_client)<br/>
<br/>

<h3><b>Assistant</b></h3>(#id_assis=>Personnel)<br/> <br>AVEC PROJECTION(Assistant, id_assis) = PROJECTION(Personnel, id_personnel),<br/>INTERSECTION (PROJECTION(Assistant, id_assis), PROJECTION(Veterinaire, id_vete)) = {}<br/>
<br/>

<h3><b>Veterinaire</b></h3>(#id_vete=>Personnel)<br/> <br>AVEC PROJECTION(Veterinaire, id_vete) = PROJECTION(Personnel, id_personnel), <br/>INTERSECTION (PROJECTION(Assistant, id_assis), PROJECTION(Veterinaire, id_vete)) = {}<br/>
<br/>

<h3><b>Veterinaire_responsable</b></h3>(date_debut:Timestamp, date_fin:Timestamp, #animal=>Animal, #veterinaire=>Veterinaire) <br/><br>AVEC date_debut NOT NULL,<br/> PROJECTION(Veterinaire_responsable, animal) = PROJECTION(Animal, id_animal), <br/>PROJECTION(Veterinaire_responsable, veterinaire) = PROJECTION(Veterinaire, id_vete)<br/>
<br/>

<h3><b>Consultation</b></h3>(date:Timestamp, personnel=>Personnel, animal=>Animal)<br/> <br>AVEC date, personnel, animal KEY, <br/>PROJECTION(Consultation, personnel) = PROJECTION(Personnel, id_personnel), <br/>PROJECTION(Consultation, animal) = PROJECTION(Animal, id_animal)<br/>
<br/>

<h3><b>Observations</b></h3>(#id_obs:int, observation:string, procedure:string, taille:int, poids:int, #consultation=>Consultation)<br/>
<br>AVEC id_obs LOCAL KEY<br><br>

<h3><b>Traitement</b></h3>(#id_traite:int, debut_traitement:Date, date_fin:Date, veterinaire=>Veterinaire, animal=>Animal)<br/> <br>AVEC debut_traitement NOT NULL, date_fin NOT NULL<br/>
<br/>

<h3><b>Analyse</b></h3>(date:Timestamp, lien:string, personnel=>Personnel, animal=>Animal) <br/><br>AVEC date, personnel, animal KEY, lien UNIQUE NOT NULL<br/> PROJECTION(Analyse, personnel) = PROJECTION(Personnel, id_personnel), <br/>PROJECTION(Analyse, animal) = PROJECTION(Animal, id_animal)<br/>
<br/>

<h3><b>Medicament</b></h3>(#nom_molecule:string, description:string)<br/> <br>AVEC description NOT NULL<br/>
<br/>

<h3><b>Prescription</b></h3>(quantite:int, #traitement=>Traitement, #medicament=>Medicament) <br/><br>AVEC PROJECTION(Prescription, traitement) = PROJECTION(Traitement, id_traite),<br/> PROJECTION(Prescription, medicament) = PROJECTION(Medicament, nom_molecule)<br/>
<br/>

<h3><b>Espece</b></h3>(#nom:type_animal)<br/>
<br/>

<h3><b>Specialite</b></h3>(#personnel=>Personnel, #espece=>Espece)<br/> <br>AVEC PROJECTION(Specialite, personnel) = PROJECTION(Personnel, id_personnel), PROJECTION(Specialite, espece) = PROJECTION(Espece, nom)<br/>
<br/>

<h3><b>Autorisation</b></h3>(#medicament=>Medicament, #espece=>Espece) <br/><br>AVEC PROJECTION(Autorisation, medicament) = PROJECTION(Medicament, nom_molecule),<br/> PROJECTION(Autorisation, espece) = PROJECTION(Espece, nom)<br/>
</div>