<h1>NDC</h1>

<ul>

<li><h2><i>Identité</i>: ID(clé), nom, prénom, date_naiss, code_postal, ville, rue, num_tel</h2></li>
<ul> 
<li>est une classe abstraite</li>
<li>composé par 2 classes filles: <ins><b>Client</b></ins> et <ins><b>Personnel</b></ins></li>
</ul>
<br/>

<li><h2>Client</h2></li>
<ul> 
<li>est une classe fille d'<ins><b>Identité</b></ins></li>
<li>est propriétaire de plusieurs <ins><b>Animal</b></ins> (1..n-*)</li>
<li>ne peut pas être <ins><b>Personnel</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Personnel </h2></li>
<ul> 
<li>est de type <strong>type_animal</strong></li>
<li>est une classe fille d'<ins><b>Identité</b></ins></li>
<li>composé par 2 classes filles: <ins><b>Assistant</b></ins> et <ins><b>Vétérinaire</b></ins></li>
<li>a pour spécialité des <ins><b>Espèce</b></ins>(*-*)</li>
<li>fait les <b>Consultation</b> sur les <ins><b>Animal</b></ins>  (*-*)</li>
<ul><li>avec date, observation, procédure, taille, poids</li></ul>
<li>fait les <b>Analyse</b> sur les <ins><b>Animal</b></ins> (*-*)</li>
<ul><li>avec date, lien</li></ul>
<li>ne peut pas être <ins><b>Client</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Assistant</h2></li>
<ul> 
<li>est une classe fille de <ins><b>Personnel</b></ins></li>
<li>ne peut pas être <ins><b>Vétérinaire</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Vétérinaire</h2></li>
<ul> 
<li>est une classe fille de <ins><b>Personnel</b></ins></li>
<li>ne peut pas être <ins><b>Assistant</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Animal: id(clé), nom, date_naiss, num_puce, num_pass</h2></li>
<ul> 
<li>est d'un <ins><b>Espèce</b></ins> (*-1)</li>
<li>a des <ins><b>Client</b></ins> comme propriétaire (1..n-*)</li>
<ul><li>avec une date de début et une date de fin</li></ul>
<li>est traité par les <ins><b>Vétérinaire </b></ins> (*-1..n)</li>
<ul><li>avec une date de début et une date de fin</li></ul>
</ul>
<br/>

<li><h2>Espèce: nom(clé)</h2></li>
<ul>
<li>est de type <b>type_animal</b></li>
</ul>
<br/>

<li><h2>Traitement: id(clé), début_traitement, date_fin </h2></li>
<ul>
<li>prescrit par un <ins><b>Vétérinaire </b></ins>(*-1)</li> 
<li>concerne à un <ins><b>Animal </b></ins>(*-1)</li> 
<li>est composé de plusieurs <ins><b>Médicament</b></ins>(*-1..n)</li> 
<ul><li>contient une prescription indiquant la quantité de chaque médicament</li></ul>        
</ul>
<br/>

<li><h2>Médicament: nom_molécule(clé), description </h2></li>
<ul>
<li>est autorisé pour certaines <ins><b>Espèce</b></ins>(*-*)</li> 
</ul>
<br/>

</ul>

<h2><strong>type_animal</strong></h2>
<ul> 
<li>félins, canidés, reptiles, rongeurs, oiseaux, autres</li>
</ul>
<br/>

<h1>Explications</h1>
<ul>
<li>
<h2>Héritage exclusif</h2>
<ul>
<li>Un Client ne peut pas être un Personnel</li>
<li>Un Personnel ne peut pas être un Client</li> 
<li>Un Assistant ne peut pas être un Vétérinaire</li>
<li>Un Vétérinaire ne peut pas être un Assistant</li> 
</ul>
</li>
<br/>

<li><h2>Le choix de ne pas mettre la classe dossier médical</h2>
<ul> 
<li>C'est pas utile d'avoir les associations 1..1 (entre Analyse et Dossier_Medical, Traitement et Dossier_Medical, Consultation et Dossier_Medical)</li> 
<li>Du coup on a remplacé les classes Analyse et Consultation par les classes d'association entre Animal et Personnel (identifié par date id_personnel et id_animal) et permit seulement le Vétérinaire prescrire un Traitement</li>
</ul></li>

<li><h2>Le choix d'utiliser héritage par les classes filles (Identité - Client et Personnel)</h2>
<ul> 
<li>Identité est une classe abstraite. </li> 
<li>Tous les attributs de la classe mère Identité sont répétés au niveau de classe fille.</li>
<li>La clé primaire de la classe mère Identité est utilisé pour identifier chacune de ses classes filles (Client et Personnel)</li>
</ul></li>

<li><h2>Le choix d'utiliser héritage par références (Personnel - Assistant et Vétérinaire)</h2>
<ul> 
<li>Chaque classe mère ou fille, est représentée par une relation</li>
<li>La clé primaire de la classe mère (Personnel) est utilisé pour identifier chacune de ses classes filles (Assistant et Vétérinaire) à la fois la clé primaire (ID) est une clé étrangère vers la classe mère</li>
</ul></li>

<li><h2>Pourquoi séparer Consultation et Analyse</h2>
<ul> 
<li>En fait, Analyse est le résultat et  le résumé de Consultation.</li>
<li>On fait Analyse après Consultation.</li>
</ul></li>
</ul>
