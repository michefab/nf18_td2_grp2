## Justifications du passage relationnel -> R-JSON

Nous avons identifié les tables contenants plusieurs attributs pouvant être NULL et les avons transformés en un attribut JSON.

**Les changements:**
- table ``Consultation`` regroupement des attributs "procédure", "taille", "poids", "observation" en 1 seul attribut "observation" de type JSON
- table ``Animal`` pour regroupement des "date_naiss", "num_puss", "num_pass" en 1 seul attribut "informations" de type JSON

Ces nouveaux attributs apparaissent dans une nouvelle table en composition avec celle d'origine. <br><br>
De plus, c'est possible de faire l'insertion et la sélection pour la partie JSON dans notre base de donnée. 

--- 

## Table Animal dans la base de donnée 

![animal](./animal.png) <br><br>


## Table Consultation dans la base de donnée 

![consultation](./consultation.png)